﻿using AutoMapper;
using AutoMapper.Configuration;
using NLog;
using System;
using DataTransaction = CustomerInquiry.Data.Transaction;
using DataCustomer = CustomerInquiry.Data.Customer;
using OutputTransaction = CustomerInquiry.Models.Output.Transaction;
using OutputCustomer = CustomerInquiry.Models.Output.Customer;
using System.Linq;
using System.Globalization;

namespace CustomerInquiry
{
    public static class AutoMap
    {
        private static bool _done = false;
        private static Logger mLog = LogManager.GetCurrentClassLogger();

        public static void Init()
        {
            try
            {
                var config = new MapperConfigurationExpression();

                InitConfig(config);

                Mapper.Initialize(config);
                Mapper.AssertConfigurationIsValid();

                _done = true;
            }
            catch (Exception ex)
            {
                mLog.Error($"Failed to initialize AutoMapper - {ex}");
                throw;
            }
        }

        public static T Map<T>(object obj)
        {
            if (!_done)
                Init();

            return Mapper.Map<T>(obj);
        }

        public static void InitConfig(MapperConfigurationExpression config)
        {
            config.CreateMap<DataTransaction, OutputTransaction>()
                .ForMember(dest => dest.id, opt => opt.ResolveUsing(x => x.Id))
                .ForMember(dest => dest.date, opt => opt.ResolveUsing(x => x.Date.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.amount, opt => opt.ResolveUsing(x => x.Amount.ToString("0.00")))
                .ForMember(dest => dest.currency, opt => opt.ResolveUsing(x => OutputTransaction.c_CurrencyCodes[x.Currency]))
                .ForMember(dest => dest.status, opt => opt.ResolveUsing(x => OutputTransaction.c_StatusCaptions[x.Status]));

            config.CreateMap<DataCustomer, OutputCustomer>()
                .ForMember(dest => dest.customerID, opt => opt.ResolveUsing(x => x.CustomerId))
                .ForMember(dest => dest.name, opt => opt.ResolveUsing(x => x.Name))
                .ForMember(dest => dest.email, opt => opt.ResolveUsing(x => x.Email))
                .ForMember(dest => dest.mobile, opt => opt.ResolveUsing(x => FormatMobile(x.Mobile)))
                .ForMember(dest => dest.transactions, 
                    opt => opt.ResolveUsing(x => x.Transactions.Take(5).Select(t => Map<OutputTransaction>(t))));
        }

        private static string FormatMobile(long mobile)
        {
            string strMobile = mobile.ToString();

            while (strMobile.Length < 10)
            {
                strMobile = "0" + strMobile;
            }

            return strMobile;
        }
    }
}