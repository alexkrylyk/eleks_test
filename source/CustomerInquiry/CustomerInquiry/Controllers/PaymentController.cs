﻿using CustomerInquiry.Helpers;
using CustomerInquiry.Models.Input;
using NLog;
using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace CustomerInquiry.Controllers
{
    public class PaymentController : ApiController
    {
        private static readonly Logger mLog = LogManager.GetCurrentClassLogger();

        private readonly IDatabaseHelper databaseHelper;
        public PaymentController(IDatabaseHelper databaseHelper)
        {
            this.databaseHelper = databaseHelper;
        }

        [HttpPost, Route("api/getcustomer")]
        [Description("Get customer by email or customerID")]
        public IHttpActionResult GetCustomer([FromBody]InquiryInput input)
        {
            try
            {
                // If input is empty
                if ((input == null) || (string.IsNullOrWhiteSpace(input.email) && (input.customerID == 0)))
                {
                    mLog.Error("No inquiry criteria");
                    return BadRequest("No inquiry criteria");
                }
                // If Email is not empty - it has to be valid
                if (!string.IsNullOrWhiteSpace(input.email) && !ValidateEmail(input.email))
                {
                    mLog.Error("Invalid Email");
                    return BadRequest("Invalid Email");
                }
                // CustomerID is not empty - it has to be valid
                if (!(input.customerID == 0) && !ValidateCustomerID(input.customerID))
                {
                    mLog.Error("Invalid Customer ID");
                    return BadRequest("Invalid Customer ID");
                }

                var customer = databaseHelper.GetCustomer(input);

                if (customer == null)
                {
                    mLog.Info($"Not found. Input: customerID = {input.email}, email = {input.email}");
                    return NotFound();
                }
                else
                {
                    return Ok(customer);
                }
            }
            catch (Exception ex)
            {
                mLog.Error($"Failed to get customer - {ex}");
                return BadRequest();
            }
        }

        #region Helper functionality

        private static bool ValidateCustomerID(long customerID)
        {
            // customerID must be a number max 10 digits
            if (customerID > 9999999999)
                return false;

            return true;
        }

        private bool ValidateEmail(string email)
        {
            const string emailPattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                   + "@"
                                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";

            return email != null && email.Length <= 25 && Regex.IsMatch(email, emailPattern);
        }

        #endregion
    }
}
