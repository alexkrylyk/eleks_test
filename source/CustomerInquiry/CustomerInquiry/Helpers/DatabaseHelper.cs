﻿using System.Linq;
using CustomerInquiry.Data;
using CustomerInquiry.Models.Input;
using DataCustomer = CustomerInquiry.Data.Customer;
using OutputCustomer = CustomerInquiry.Models.Output.Customer;

namespace CustomerInquiry.Helpers
{
    public class DatabaseHelper : IDatabaseHelper
    {
        public OutputCustomer GetCustomer(InquiryInput input)
        {
            using (var db = new PaymentEntitiesContainer())
            {
                DataCustomer customer = null;

                // If both params are present - filter by both
                if (input.customerID > 0 && !string.IsNullOrWhiteSpace(input.email))
                {
                    customer = db.Customers.Where(c =>
                        c.CustomerId == input.customerID && c.Email.Equals(input.email)).SingleOrDefault();
                }
                // If only customerID present
                else if (input.customerID > 0)
                {
                    customer = db.Customers.Where(c => c.CustomerId == input.customerID).SingleOrDefault();
                }
                // If only email present
                else
                {
                    customer = db.Customers.Where(c => c.Email.Equals(input.email)).SingleOrDefault();
                }

                return AutoMap.Map<OutputCustomer>(customer);
            }
        }
    }
}