﻿using OutputCustomer = CustomerInquiry.Models.Output.Customer;
using CustomerInquiry.Models.Input;

namespace CustomerInquiry.Helpers
{
    public interface IDatabaseHelper
    {
        OutputCustomer GetCustomer(InquiryInput input);
    }
}
