﻿namespace CustomerInquiry.Models.Input
{
    public class InquiryInput
    {
        public long customerID { get; set; }
        public string email { get; set; }
    }
}