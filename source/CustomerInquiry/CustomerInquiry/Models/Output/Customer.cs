﻿using System.Collections.Generic;

namespace CustomerInquiry.Models.Output
{
    public class Customer
    {
        public long customerID { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public List<Transaction> transactions { get; set; }
    }
}