﻿namespace CustomerInquiry.Models.Output
{
    public class Transaction
    {
        public static readonly string[] c_CurrencyCodes = { "USD", "JPY", "THB", "SGD" };
        public static readonly string[] c_StatusCaptions = { "Success", "Failed", "Canceled" };

        public int id { get; set; }
        public string date { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string status { get; set; }
    }
}