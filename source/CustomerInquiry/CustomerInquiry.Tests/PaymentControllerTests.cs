﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerInquiry.Helpers;
using CustomerInquiry.Models.Input;
using Moq;
using CustomerInquiry.Controllers;
using OutputCustomer = CustomerInquiry.Models.Output.Customer;
using System.Web.Http.Results;

namespace CustomerInquiry.Tests
{
    [TestClass]
    public class PaymentControllerTests
    {
        [TestMethod]
        public void GetCustomerByCustomerIDAndEmailSuccess()
        {
            // Arrange
            var input = new InquiryInput
            {
                customerID = 123456,
                email = "user@domain.com"
            };

            var output = new OutputCustomer
            {
                customerID = 123456,
                name = "Firstname Lastname",
                email = "user@domain.com",
                mobile = "0123456789"
            };

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var customer = (controller.GetCustomer(input) as OkNegotiatedContentResult<OutputCustomer>).Content;

            // Assert
            Assert.AreEqual(input.customerID, customer.customerID);
            Assert.AreEqual(input.email, customer.email);
        }

        [TestMethod]
        public void GetCustomerByCustomerIDAndEmailNotFound()
        {
            // Arrange
            var input = new InquiryInput
            {
                customerID = 999999,
                email = "user234@domain.com"
            };

            OutputCustomer output = null;

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var result = controller.GetCustomer(input);

            // Assert
            Assert.AreEqual(result.GetType(), typeof(NotFoundResult));
        }

        [TestMethod]
        public void GetCustomerByCustomerIDSuccess()
        {
            // Arrange
            var input = new InquiryInput
            {
                customerID = 123456
            };

            var output = new OutputCustomer
            {
                customerID = 123456,
                name = "Firstname Lastname",
                email = "user@domain.com",
                mobile = "0123456789"
            };

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var customer = (controller.GetCustomer(input) as OkNegotiatedContentResult<OutputCustomer>).Content;

            // Assert
            Assert.AreEqual(input.customerID, customer.customerID);
        }

        [TestMethod]
        public void GetCustomerByCustomerIDNotFound()
        {
            // Arrange
            var input = new InquiryInput
            {
                customerID = 999999
            };

            OutputCustomer output = null;

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var result = controller.GetCustomer(input);

            // Assert
            Assert.AreEqual(result.GetType(), typeof(NotFoundResult));
        }

        [TestMethod]
        public void GetCustomerByEmailSuccess()
        {
            // Arrange
            var input = new InquiryInput
            {
                email = "user@domain.com"
            };

            var output = new OutputCustomer
            {
                customerID = 123456,
                name = "Firstname Lastname",
                email = "user@domain.com",
                mobile = "0123456789"
            };

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var customer = (controller.GetCustomer(input) as OkNegotiatedContentResult<OutputCustomer>).Content;

            // Assert
            Assert.AreEqual(input.email, customer.email);
        }

        [TestMethod]
        public void GetCustomerByEmailNotFound()
        {
            // Arrange
            var input = new InquiryInput
            {
                email = "user234@domain.com"
            };

            OutputCustomer output = null;

            var mockDatabaseHelper = new Mock<IDatabaseHelper>();
            mockDatabaseHelper.Setup(m => m.GetCustomer(input)).Returns(output);

            var controller = new PaymentController(mockDatabaseHelper.Object);

            // Act
            var result = controller.GetCustomer(input);

            // Assert
            Assert.AreEqual(result.GetType(), typeof(NotFoundResult));
        }
    }
}
