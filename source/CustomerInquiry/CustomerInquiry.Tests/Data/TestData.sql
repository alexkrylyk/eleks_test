-- Customers
INSERT INTO [dbo].[Customers] (CustomerId, Name, Email, Mobile)
VALUES (123456, 'Firstname Lastname', 'user@domain.com', 123456789);

INSERT INTO [dbo].[Customers] (CustomerId, Name, Email, Mobile)
VALUES (23324, 'Firstname2 Lastname2', 'user2@domain.com', 123456789);

INSERT INTO [dbo].[Customers] (CustomerId, Name, Email, Mobile)
VALUES (978989, 'Firstname3 Lastname3', 'user3@domain.com', 123456789);

-- Transactions
INSERT INTO [dbo].[Transactions] (Date, Amount, Currency, Status, CustomerId)
VALUES (convert(datetime,'18-07-19 4:17:50 PM',5), 1234.56, 0, 0, 1);

INSERT INTO [dbo].[Transactions] (Date, Amount, Currency, Status, CustomerId)
VALUES (convert(datetime,'18-07-19 4:17:50 PM',5), 2232.56, 2, 0, 1);

INSERT INTO [dbo].[Transactions] (Date, Amount, Currency, Status, CustomerId)
VALUES (convert(datetime,'18-07-19 4:17:50 PM',5), 66.56, 0, 0, 2);
