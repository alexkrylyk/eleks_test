﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using CustomerInquiry.Data;
using System.Net.Http;
using CustomerInquiry.Models.Input;
using OutputCustomer = CustomerInquiry.Models.Output.Customer;
using System.Text;
using Newtonsoft.Json;
using CustomerInquiry.Tests.Models;

namespace CustomerInquiry.Tests
{
    [TestClass]
    public class IntegrationTests
    {
        private static readonly HttpClient client = new HttpClient();
        private const string c_Url = "http://localhost:55627/api/getcustomer";

        [TestMethod]
        public void InitDatabase()
        {
            using (var db = new PaymentEntitiesContainer())
            {
                string filePath = Assembly.GetExecutingAssembly().Location;

                // Init database
                string sqlSchema = System.IO.File.ReadAllText(filePath.Replace("CustomerInquiry.Tests.dll", "Data/PaymentEntities.edmx.sql"));
                sqlSchema = sqlSchema.Replace("GO", string.Empty);
                db.Database.ExecuteSqlCommand(sqlSchema);

                // Fill database with test data
                string sqlData = System.IO.File.ReadAllText(filePath.Replace("CustomerInquiry.Tests.dll", "Data/TestData.sql"));
                db.Database.ExecuteSqlCommand(sqlData);
            }
        }

        [TestMethod]
        public void RequestScenario1()
        {
            var input = new InquiryInput
            {
                customerID = 978989
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var responseContent = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult()
                .Content.ReadAsStringAsync().GetAwaiter().GetResult();

            OutputCustomer customer = JsonConvert.DeserializeObject<OutputCustomer>(responseContent);

            Assert.AreEqual(input.customerID, customer.customerID);
            Assert.AreEqual(customer.transactions.Count, 0);
        }

        [TestMethod]
        public void RequestScenario2()
        {
            var input = new InquiryInput
            {
                email = "user2@domain.com"
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var responseContent = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult()
                .Content.ReadAsStringAsync().GetAwaiter().GetResult();

            OutputCustomer customer = JsonConvert.DeserializeObject<OutputCustomer>(responseContent);

            Assert.AreEqual(input.email, customer.email);
            Assert.AreEqual(customer.transactions.Count, 1);
        }

        [TestMethod]
        public void RequestScenario3()
        {
            var input = new InquiryInput
            {
                customerID = 123456,
                email = "user@domain.com"
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var responseContent = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult()
                .Content.ReadAsStringAsync().GetAwaiter().GetResult();

            OutputCustomer customer = JsonConvert.DeserializeObject<OutputCustomer>(responseContent);

            Assert.AreEqual(input.customerID, customer.customerID);
            Assert.AreEqual(input.email, customer.email);
            Assert.AreEqual(customer.transactions.Count, 2);
        }

        [TestMethod]
        public void WrongEmail()
        {
            var input = new InquiryInput
            {
                email = "userdomain.com"
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "Invalid Email");
        }

        [TestMethod]
        public void WrongEmailCorrectCustomerID()
        {
            var input = new InquiryInput
            {
                email = "userdomain.com",
                customerID = 123456
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "Invalid Email");
        }

        [TestMethod]
        public void WrongCustomerID()
        {
            var input = new InquiryInput
            {
                customerID = 123456789999
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "Invalid Customer ID");
        }

        // Wrong customerID correct email
        [TestMethod]
        public void WrongCustomerIDCorrectEmail()
        {
            var input = new InquiryInput
            {
                customerID = 123456789999,
                email = "user@domain.com",
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "Invalid Customer ID");
        }

        [TestMethod]
        public void WrongCustomerIDWrongEmail()
        {
            var input = new InquiryInput
            {
                customerID = 123456789999,
                email = "userdomain.com",
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "Invalid Email");
        }

        [TestMethod]
        public void EmptyCustomerIDEmptyEmail()
        {
            var input = new InquiryInput();

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "No inquiry criteria");
        }

        [TestMethod]
        public void NoInquiryCriteria()
        {
            var response = client.PostAsync(c_Url, new StringContent("")).GetAwaiter().GetResult();
            var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

            ResponseMessage responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(responseContent);

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.BadRequest);
            Assert.AreEqual(responseMessage.Message, "No inquiry criteria");
        }

        [TestMethod]
        public void NotFoundByCustomerID()
        {
            var input = new InquiryInput
            {
                customerID = 9999
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.NotFound);
        }

        [TestMethod]
        public void NotFoundByEmail()
        {
            var input = new InquiryInput
            {
                email = "notfound@domain.com"
            };

            var requestContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json");

            var response = client.PostAsync(c_Url, requestContent).GetAwaiter().GetResult();

            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.NotFound);
        }
    }
}
